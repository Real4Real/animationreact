import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Header from './components/header/header.jsx';

export default class Main extends Component {
    render() {
        return (
            <div className="main-container">
                <Header />
            </div>
        )
    }
}
ReactDOM.render(<Main />, document.getElementById('root'))
